
			function DrawLine(ctx,x1,y1,x2,y2,color)
			{
				//console.log("Line x="+x1 + " y="+y1+" ,x2="+x2 + " y2="+y2)
				oldstroke = ctx.strokeStyle;
				oldline = ctx.lineWidth;
				ctx.strokeStyle=color;
				//console.log(ctx.strokeStyle+","+ctx.fillStyle)
				ctx.lineWidth=10;
				ctx.beginPath();
				ctx.moveTo(x1,y1);
				ctx.lineTo(x2,y2);
				ctx.stroke();
				
				ctx.strokeStyle=oldstroke;
				ctx.lineWidth=oldline;
			}
			function DrawCircle(ctx,x,y,r,color)
			{
				//console.log("Circle x="+x + " y="+y+" r="+r)
				oldstroke = ctx.strokeStyle;
				oldfill = ctx.fillStyle;
				oldline = ctx.lineWidth;
				ctx.strokeStyle=color;
				ctx.fillStyle=color;
				//console.log(ctx.strokeStyle+","+ctx.fillStyle)
				ctx.lineWidth=10;
				ctx.beginPath();
				ctx.arc(x,y,r,0,2*Math.PI);
				ctx.stroke();
				ctx.fill();
				ctx.strokeStyle=oldstroke;
				ctx.fillStyle=oldfill;
				ctx.lineWidth=oldline;
				//console.log(ctx.strokeStyle+","+ctx.fillStyle)
			}
			function DrawText(ctx,text,x,y,color)
			{
				oldfill = ctx.fillStyle;
				ctx.font = "30px Open Sans";
				ctx.fillStyle=color;
				ctx.fillText(text,x,y);
				ctx.fillStyle=oldfill;
			}
			function DrawStep(num,prog,name)
			{
				if(prog=="comp")
				{
					color="green";
				}
				else if(prog=="doing")
				{
					color="#38C6E6";
				}
				else if(prog=="waiting")
				{
					color="#999";
				}
				else if(prog=="failed")
				{
					color="#FF1919";
				}
				var c = document.getElementById(name);
				var ctx = c.getContext("2d");
				var x=25;
				var y=80*(num-1);
				DrawLine(ctx,x,y,x,y+60,color);
				y+=60;
				DrawCircle(ctx,x,y+10,10,color);
				ctx.strokeStyle="black";
				ctx.fillStyle="black";
				doinglist = ["","CLONING FROM REPO","BUNDLING INSTALL","MIGRATING DATABASE","ASSET PRECOMPILING","COMPLETE"];
				completelist = ["","CLONED FROM REPO","BUNDLED INSTALL","MIGRATED DATABASE","ASSET PRECOMPILED","COMPLETED"];

				DrawText(ctx,"Step " + num + " " +completelist[num],x+30,y+18,"#333");
			}

			function DrawProgress(name,ProgList)
			{
				DrawStep(5,ProgList[4],name);
				DrawStep(4,ProgList[3],name);
				DrawStep(3,ProgList[2],name);
				DrawStep(2,ProgList[1],name);
				DrawStep(1,ProgList[0],name);
			}